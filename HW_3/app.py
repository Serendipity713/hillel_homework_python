import sqlite3

from flask import Flask, render_template, request
from sqlalchemy.orm import sessionmaker

from db import engine
from forms import AuthorForm, GenreForm, BookForm, session
from models import Author, Genre, Book

app = Flask(__name__, template_folder='templates')


def create_form_handler(form_class, model_class, title):
    form = form_class()
    success = False
    if request.method == 'POST':
        form = form_class(request.form)
        if form.validate():
            obj = model_class()
            form.populate_obj(obj)
            session.add(obj)
            session.commit()
            success = True

    return render_template(
        'create_books.html', **{
            'form': form,
            'title': title,
            'success': success
        }
    )


@app.route('/', methods=['GET', 'POST'])
def show_books():
    books = session.query(Book).all()
    if request.method == 'POST' and 'book_year' in request.form and request.form['book_year'].isdigit():
        books = session.query(Book).filter(Book.year == request.form['book_year'])

    if request.method == 'POST' and 'book_genre_id' in request.form and request.form['book_genre_id'].isdigit():
        genre = session.query(Genre).filter(Genre.id == request.form['book_genre_id']).one()
        books = session.query(Book).filter(Book.genre_obj == genre)

    return render_template(
        'index.html', **{'books': books}
    )


@app.route('/create/book/', methods=['GET', 'POST'])
def create_book():
    return create_form_handler(BookForm, Book, "Add Book")


@app.route('/create/author/', methods=['GET', 'POST'])
def add_author():
    return create_form_handler(AuthorForm, Author, 'Add Author')


@app.route('/create/genre/', methods=['GET', 'POST'])
def add_genre():
    return create_form_handler(GenreForm, Genre, 'Add Genre')


@app.route('/update/book/<book_id>/', methods=['GET', 'POST'])
def update_book(book_id):
    tmp_book = session.query(Book).filter(Book.id == book_id).one()
    form = BookForm(obj=tmp_book)
    success = False
    if request.method == 'POST':
        form = BookForm(request.form)
        if form.validate():
            form.populate_obj(tmp_book)
            session.add(tmp_book)
            session.commit()
            success = True
    return render_template(
        'create_books.html', **{
            'form': form,
            'title': "Update book",
            'success': success
        }
    )


if __name__ == '__main__':
    app.run(debug=True)
