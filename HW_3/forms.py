from sqlalchemy.orm import sessionmaker, scoped_session
from wtforms.ext.sqlalchemy.orm import model_form
from models import Book, Author, Genre

from db import engine

# session_class = sessionmaker(bind=engine)
# session = session_class()

# session = sessionmaker(bind=engine)()
session = scoped_session(sessionmaker(bind=engine))


BookForm = model_form(Book, db_session=session)
AuthorForm = model_form(Author)
GenreForm = model_form(Genre)
