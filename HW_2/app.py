from flask import Flask, render_template, request
from faker import Faker
import requests
import json

fake = Faker()

app = Flask(__name__)


@app.route('/index/')
def home():
    return render_template(
        'index.html'
    )


@app.route('/requirements/')
def get_requirements():
    with open('static/requirements.txt') as file:
        content = file.readlines()
        return render_template(
            'requirements.html', content=content
        )


@app.route('/generate-users/', methods=['POST', 'GET'])
def generate_users():
    users = []
    number_of_users = 100
    if request.method == 'POST' and request.form['number_of_users'].isdigit():
        number_of_users = int(request.form['number_of_users'])
    for el in range(number_of_users):
        user = fake.name(), fake.email()
        user = ','.join(user)
        users.append(user)
    return render_template(
        'generate-users.html', users=users, number_of_users=number_of_users
    )


@app.route('/space/', methods=['POST', 'GET'])
def space():
    response = requests.get("http://api.open-notify.org/astros.json")
    if response.status_code == 200:
        data = response.json()
        number = data['number']
        return render_template(
            'space.html', number=number
        )


if __name__ == "__main__":
    app.run(debug=True)


