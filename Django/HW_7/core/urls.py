from django.urls import path

from .views import StudentDeleteView, StudentList, StudentUpdateView, TeacherCreateView, TeacherDeleteView, \
    TeacherList, TeacherUpdateView

urlpatterns = [
    path('', TeacherList.as_view(), name='home'),
    path('list_of_student/', StudentList.as_view(), name='students'),
    path('update_student/<int:pk>/', StudentUpdateView.as_view(), name='student_upd'),
    path('delete_student/<int:pk>/', StudentDeleteView.as_view(), name='student_del'),
    path('create_teacher/', TeacherCreateView.as_view(), name='create_teacher'),
    path('update_teacher/<int:pk>/', TeacherUpdateView.as_view(), name='teacher_upd'),
    path('delete_teacher/<int:pk>/', TeacherDeleteView.as_view(), name='teacher_del')
]
