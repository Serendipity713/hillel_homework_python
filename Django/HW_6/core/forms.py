from django import forms

from .models import Group, Student


class GroupCreateForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = '__all__'
        widgets = {
            'teacher': forms.widgets.RadioSelect()
        }


class StudentCreateForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = '__all__'
        widgets = {
            'group': forms.widgets.CheckboxSelectMultiple()
        }
