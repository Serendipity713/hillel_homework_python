from django.db.models import Avg, Count, Max, Min
from django.urls import reverse_lazy
from django.views.generic import CreateView, FormView, TemplateView

from .forms import GroupCreateForm, StudentCreateForm
from .models import Group, Teacher


class ListGroupView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        groups_data = Group.objects.all().select_related().annotate(
            students_count=Count('student'),
            students_avg_age=Avg('student__age'),
            students_max_age=Max('student__age'),
            students_min_age=Min('student__age')

        )
        context = {
            'groups_data': groups_data
        }
        return context


class GroupCreateView(FormView):
    template_name = 'create.html'
    form_class = GroupCreateForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        form.save()
        return super(GroupCreateView, self).form_valid(form)


class StudentCreateView(FormView):
    template_name = 'create.html'
    form_class = StudentCreateForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        form.save()
        return super(StudentCreateView, self).form_valid(form)


class TeacherCreateView(CreateView):
    template_name = 'create.html'
    success_url = reverse_lazy('home')
    model = Teacher
    fields = '__all__'
