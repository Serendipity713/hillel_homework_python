from django.urls import path

from .views import GroupCreateView, ListGroupView, StudentCreateView, TeacherCreateView

urlpatterns = [
    path('', ListGroupView.as_view(), name='home'),
    path('create_group/', GroupCreateView.as_view(), name='create_group'),
    path('create_student/', StudentCreateView.as_view(), name='create_student'),
    path('create_teacher/', TeacherCreateView.as_view(), name='create_teacher')
]
