from django.db import models


class Teacher(models.Model):
    last_name = models.CharField(max_length=255)
    first_name = models.CharField(max_length=255)
    age = models.IntegerField()

    def __str__(self):
        return self.last_name
