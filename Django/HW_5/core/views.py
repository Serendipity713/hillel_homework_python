from django.views.generic import TemplateView

from .models import Teacher


class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        teachers = Teacher.objects.all()
        context = {'teachers': teachers}

        if 'first_name' in self.request.GET:
            context['first_name'] = self.request.GET['first_name']
            teachers = teachers.filter(
                first_name__contains=self.request.GET['first_name']
            )
        if 'last_name' in self.request.GET:
            context['last_name'] = self.request.GET['last_name']
            teachers = teachers.filter(
                last_name__contains=self.request.GET['last_name']
            )

        if 'age' in self.request.GET:
            context['age'] = self.request.GET['age']
            teachers = teachers.filter(
                age=self.request.GET['age']
            )

            context['teachers'] = teachers
        return context
