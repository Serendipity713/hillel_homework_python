from core.models import Teacher

from django.core.management.base import BaseCommand

from faker import Faker

fake = Faker()


class Command(BaseCommand):

    def handle(self, *args, **options):
        teachers = []
        for el in range(100):
            teacher = Teacher(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                age=fake.random_int(min=17, max=100)
            )
            teachers.append(teacher)
        Teacher.objects.bulk_create(teachers)
