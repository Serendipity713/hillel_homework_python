from django.contrib import admin

from .models import Group, Student, Teacher


class StudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'age', 'group', 'phone')
    search_fields = ('last_name',)
    list_filter = ('group',)


admin.site.register(Student, StudentAdmin)
admin.site.register(Teacher)
admin.site.register(Group)
