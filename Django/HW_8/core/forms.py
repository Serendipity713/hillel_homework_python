from django import forms

from .fields import PhoneNumberField
from .models import Student


class StudentForm(forms.ModelForm):
    phone = PhoneNumberField()

    class Meta:
        model = Student
        fields = '__all__'

    def save(self, commit=True):
        return super(StudentForm, self).save(commit)
