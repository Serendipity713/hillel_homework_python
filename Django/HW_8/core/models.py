from django.db import models
from django.db.models.signals import pre_save

from phonenumber_field.modelfields import PhoneNumberField


class Teacher(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)

    def __str__(self):
        return self.last_name


class Group(models.Model):
    name = models.CharField(max_length=255)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE, related_name='groups')

    def __str__(self):
        return self.name


class Student(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    age = models.PositiveIntegerField()
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name='students')
    phone = PhoneNumberField(blank=True)

    def __str__(self):
        return self.last_name


def capitalize_signal(sender, instance, **kwargs):
    instance.first_name = instance.first_name.capitalize()
    instance.last_name = instance.last_name.capitalize()


pre_save.connect(capitalize_signal, sender=Teacher)
pre_save.connect(capitalize_signal, sender=Student)
