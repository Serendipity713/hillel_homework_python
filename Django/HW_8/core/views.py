from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from .forms import StudentForm
from .models import Student, Teacher


class StudentList(ListView):
    model = Student
    context_object_name = 'students'

    def get_queryset(self):
        return Student.objects.all().select_related()


class StudentCreateView(CreateView):
    template_name = 'create.html'
    success_url = reverse_lazy('home')
    model = Student
    form_class = StudentForm


class StudentUpdateView(UpdateView):
    template_name = 'student_update.html'
    success_url = reverse_lazy('students')
    model = Student
    form_class = StudentForm


class StudentDeleteView(DeleteView):
    template_name = 'delete.html'
    success_url = reverse_lazy('home')
    model = Student
    fields = '__all__'


class TeacherList(ListView):
    model = Teacher
    context_object_name = 'teachers'

    def get_queryset(self):
        return Teacher.objects.all().prefetch_related('groups')


class TeacherCreateView(CreateView):
    template_name = 'create.html'
    success_url = reverse_lazy('home')
    model = Teacher
    fields = '__all__'


class TeacherUpdateView(UpdateView):
    template_name = 'update.html'
    success_url = reverse_lazy('home')
    model = Teacher
    fields = '__all__'


class TeacherDeleteView(DeleteView):
    template_name = 'delete.html'
    success_url = reverse_lazy('home')
    model = Teacher
    fields = '__all__'
