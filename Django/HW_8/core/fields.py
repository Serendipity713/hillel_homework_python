from django import forms


class PhoneNumberField(forms.Field):

    def clean(self, value):
        if value[0] != '+':
            raise forms.ValidationError("First symbol should be +")

        return super(PhoneNumberField, self).clean(value)

    def validate(self, value):
        if not value[1:].isdigit():
            raise forms.ValidationError("Should be a numbers")

        elif len(value) < 10 or len(value) > 13:
            raise forms.ValidationError('Phone number length not valid')
        return value
