from datetime import datetime, timedelta

from HW_9 import celery_app

from django.core.mail import send_mail

from .models import Logger


@celery_app.task
def send_mail_task(title, message, email):
    send_mail(subject=title, message=message, recipient_list=['admin@example.com'], from_email=email)
    # print("Email was sent!")


@celery_app.task
def delete_old_logger():
    Logger.objects.filter(date_create__gte=datetime.now() - timedelta(days=7)).delete()
    # print('Deleted objects older than 7 days')
