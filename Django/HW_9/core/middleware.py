import time

from .models import Logger


class LogMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        t1 = time.time()
        response = self.get_response(request)
        ex_time = time.time() - t1
        if request.path != '/admin/':
            Logger(
                path=request.path,
                method=request.method,
                execution_time=ex_time
            ).save()
        return response
