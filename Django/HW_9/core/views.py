from django.urls import reverse_lazy
from django.views.generic import FormView

from .forms import ContactUsForm
from .tasks import send_mail_task


class ContactUsView(FormView):
    template_name = 'contact.html'
    form_class = ContactUsForm
    # fields = '__all__'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        response = super(ContactUsView, self).form_valid(form)
        send_mail_task.delay(form.cleaned_data['title'], form.cleaned_data['message'], form.cleaned_data['email_from'])
        return response
