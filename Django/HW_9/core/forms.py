from django import forms


class ContactUsForm(forms.Form):
    title = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea, required=False)
    email_from = forms.EmailField()
