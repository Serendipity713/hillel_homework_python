from django.db import models


class Logger(models.Model):
    path = models.FileField()
    method = models.CharField(max_length=6)
    execution_time = models.DateTimeField()
    date_create = models.DateTimeField(auto_now_add=True)
